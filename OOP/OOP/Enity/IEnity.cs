﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Enity
{
    public interface IEnity
    {
        int Id { get; set; }
        string Name { get; set; }
    }
}
