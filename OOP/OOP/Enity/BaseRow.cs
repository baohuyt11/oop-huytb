﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Enity
{
    public class BaseRow : IEnity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
