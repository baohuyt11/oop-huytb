﻿using OOP.Enity;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.DAO
{
    public class Database
    {
        private List<Product> ProductTable = new List<Product>();
        private List<Category> CategoryTable = new List<Category>();
        private List<Accessory> AccessoryTable = new List<Accessory>();

        private static Database instance = null;
        private static Database() { }
        public static Database Instance
        {                       
            get 
            {
                if (instance == null)
                {
                    instance = new Database();
                }   
                return instance;
            }
        }
        
        
        public int insertTable(string name, object row)
        {
            int checkInsert = 0;
            if (name == "ProductTable")
            {
                checkInsert = 1;
                ProductTable.Add((Product)row);
            }
            else if (name == "CategoryTable")
            {
                checkInsert = 1;
                CategoryTable.Add((Category)row);
            }
            else if (name == "AccessoryTable")
            {
                checkInsert = 1;
                AccessoryTable.Add((Accessory)row);
            }
            return checkInsert;

        }

       
        public object selectTable(string name, string where)
        {
            if(name == "ProductTable")
            {
                foreach(Product p in ProductTable)
                {
                    if (p.Name == where)
                    {
                        return (object)p;
                    }
                }
            }
            if(name == "CategoryTable")
            {
                foreach (Category c in CategoryTable)
                {
                    if (c.Name == where)
                    {
                        return (object)c;
                    }
                }
            }
            if (name == "AccessoryTable")
            {
                foreach (Accessory a in AccessoryTable)
                {
                    if (a.Name == where)
                    {
                        return (object)a;
                    }
                }
            }
        }

        
        public int updateTable(string name, object row)
        {
            int checkUpdate = 0;
            if (name == "ProductTable")
            {
                Product ProductUpdate = (Product)row;
                foreach (Product p in ProductTable)
                {
                    if (p.Id == ProductUpdate.Id)
                    {
                        p.Name = ProductUpdate.Name;
                        p.CategoryId = ProductUpdate.CategoryId;
                        checkUpdate = 1; 
                    }
                }
                return checkUpdate;

            }
            if (name == "CategoryTable")
            {
                Category CategoryUpdate = (Category)row;
                foreach (Category p in CategoryTable)
                {
                    if (p.Id == CategoryUpdate.Id)
                    {
                        p.Name = CategoryUpdate.Name;
                        checkUpdate = 1;                        
                    }
                }
                return checkUpdate;
            }
            if(name == "AccessoryTable")
            {
                Accessory AccessoryUpdate = (Accessory)row;
                foreach (Accessory p in AccessoryTable)
                {
                    if (p.Id == AccessoryUpdate.Id)
                    {
                        p.Name = AccessoryUpdate.Name;
                        checkUpdate = 1;                        
                    }
                }
                return checkUpdate;
            }
        }

        public int updateTableById(int id, object row)
        {
            int checkUpdate = 0;
            Product product = new Product();
            Category category = new Category();
            Accessory accessory = new Accessory();

            if (product == row)
            {
                foreach (Product p in ProductTable)
                {
                    if (p.Id == id)
                    {
                        p.Name = product.Name;
                        p.CategoryId = product.CategoryId;
                        checkUpdate = 1;
                    }
                }
                return checkUpdate;
            }
            if (category == row)
            {
                foreach (Category c in CategoryTable)
                {
                    if (c.Id == id)
                    {
                        c.Name = product.Name;
                        checkUpdate = 1;
                    }
                }
                return checkUpdate;
            }
            if (accessory == row)
            {
                foreach (Accessory a in AccessoryTable)
                {
                    if (a.Id == id)
                    {
                        a.Name = product.Name;
                        checkUpdate = 1;
                    }
                }
                return checkUpdate;
            }
        }

        public bool deleteTable(string name, object row)
        {
            bool checkDelete = false;
            if (name == "ProductTable")
            {
                Product ProductDelete = (Product)row;
                foreach (Product product in ProductTable)
                {
                    if (product.Id == ProductDelete.Id)
                    {
                        ProductTable.Remove(product);
                        checkDelete = true;
                    }
                }
                return checkDelete;
            }

            if (name == "CategoryTable")
            {
                Category CategoryDelete = (Category)row;
                foreach (Category category in CategoryTable)
                {
                    if (category.Id == CategoryDelete.Id)
                    {
                        CategoryTable.Remove(category);
                        checkDelete = true;
                    }
                }
                return checkDelete;
            }
            if (name == "AccessoryTable")
            {
                Accessory AccessoryDelete = (Accessory)row;
                foreach (Accessory accessory in AccessoryTable)
                {
                    if (accessory.Id == AccessoryDelete.Id)
                    {
                        AccessoryTable.Remove(accessory);
                        checkDelete = true;
                    }
                }
                return checkDelete;
            }
        }

        public List<object> findAllTable(string Name)
        {
            if (Name == "ProductTest")
            {
                foreach (Product product in ProductTable)
                {
                    return (object)product;
                }
            }
            if (Name == "CategoryTest")
            {
                foreach (Category category in CategoryTable)
                {
                    return (object)category;
                }
            }
            if (Name == "Accessory")
            {
                foreach (Accessory accessory in AccessoryTable)
                {
                    return (object)accessory;
                }
            }
        }

        
        public void truncateTable(string name)
        {
            if (name == "ProductTable")
            {
                ProductTable.Clear();
            }
            if (name == "CategoryTable")
            {
                CategoryTable.Clear();
            }
            if (name == "AccessoryTable")
            {
                AccessoryTable.Clear();
            }
        }
    }
}
