﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.DAO
{
	public interface IDao
	{
		bool Insert(object t);

		bool Update(object t);

		bool Delete(object t);

		List<object> FindAll();
	}
}
