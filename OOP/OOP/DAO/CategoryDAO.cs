﻿using OOP.Enity;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.DAO
{
    class CategoryDAO : BaseDao<Accessory>
    {
        public string TableName 
        { 
            get { return TableName; } 
            set { TableName = "Category"; } 
        }
    }
}
