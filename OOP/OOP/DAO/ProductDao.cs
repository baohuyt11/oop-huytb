﻿using OOP.Enity;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.DAO
{
    class ProductDao : BaseDao<Accessory>
    {
        public string TableName 
        { 
            get { return TableName; } 
            set { TableName = "Product"; } 
        }
    }
}
