﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.DAO
{
    abstract class BaseDao<object> : IDao<object>
    {
        public virtual string TableName { get; set; }

        /// <summary>
        /// insert function
        /// Created by: HuyTB (11/8/2022)
        /// </summary>
        public bool insert(object row)
        {
            bool status = false;
            int checkInsert = Database.Instance().insertTable($"{TableName}Table", row);
            if (checkInsert == 1)
            {
                status = true;
            }
            else if (checkInsert == 0)
            {
                status = false;
            }
            return status;
        }

        /// <summary>
        /// update function
        /// Created by: HuyTB (11/8/2022)
        /// </summary>
        public bool update(object row)
        {
            bool status = false;
            int checckUpdate = Database.Instance().updateTable($"{TableName}Table", row);
            if (checckUpdate == 1)
            {
                status = true;
            }
            else if (checckUpdate == 0)
            {
                status = false;
            }
            return status;
        }

        /// <summary>
        /// delete function
        /// Created by: HuyTB (11/8/2022)
        /// </summary>
        public bool delete(object row)
        {
            bool checckDelete = Database.Instance().deleteTable($"{TableName}Table", row);
            return checckDelete;
        }

        /// <summary>
        /// find all function
        /// Created by: HuyTB (11/8/2022)
        /// </summary>
        public object findAll()
        {
            List<object> objects = new List<object>();
            objects = Database.Instance().findAllTable($"{TableName}Table");
            return objects;
        }

        /// <summary>
        /// find all by Id function
        /// Created by: HuyTB (11/8/2022)
        /// </summary>
        public object findById(int name)
        {
            List<object> objects = new List<object>();
            foreach (Objects o in Database.Instance().findAllTable($"{TableName}Table"));
            {
                if (o.Id == name)
                {
                    objects.Add(o);
                }
            }
            return objects;
        }
    }
}
