﻿using OOP.Enity;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Demo
{
    class ProductDemo
    {
        public Product product;
        /// <summary>
        /// Test create product function
        /// Created by: HuyTB (10/8/2022)
        /// </summary>
        public void createProductTest()
        {
            product = new Product() { Id = 1, Name = "Hii", CategoryId = 2 };
        }
        /// <summary>
        /// Test print product function
        /// Created by: HuyTB (10/8/2022)
        /// </summary>
        public void printProduct(Product product)
        {
            Console.Write("ID: " + product.Id + "Name: " + product.Name + "CategoryId: " + product.CategoryId);
        }
    }
}
