﻿using OOP.DAO;
using OOP.Enity;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Demo
{
    public class DatabaseDemo
    {
        public void insertTableTest()
        {
            Product product = new Product() { Id = 1, Name = "Hii", CategoryId = 1 };
            Database.Instance().insertTable("ProductTable", product);

            Category category = new Category() { Id = 1, Name = "BanhBao" };
            Database.Instance().insertTable("CategoryTable", category);

            Accessory accessory = new Accessory() { Id = 1, Name = "HaCao" };
            Database.Instance().insertTable("AccessoryTable", accessory);
        }

        
        public void selectTableTest()
        {
            Database.Instance().selectTable("ProductTable", "Hii");
            printTableTest();
        }

        public void updateTableTest()
        {

            Product ProductUpdate = new Product() { Id = 1, Name = "Hee", CategoryId = 1 };
            Database.Instance().updateTable("ProductTable", ProductUpdate);
            printTableTest();
        }

        public void updateTableByIdTest()
        {
            Product ProductUpdateById = new Product() { Id = 1, Name = "Hee", CategoryId = 1 };
            Database.Instance().updateTableById(1, ProductUpdateById);
            printTableTest();
        }

        public void deleteTableTest()
        {
            Console.WriteLine("Before");
            printTableTest();

            Product ProductDelete = new Product() { Id = 2, Name = "BaCo", CategoryId = 1 };
            Database.Instance().deleteTable("CategoryTable", ProductDelete);

            Console.WriteLine("After");
            printTableTest();
        }
        public void truncateTableTest()
        {
            Database.Instance().truncateTable("ProductTable");
            printTableTest();
        }

        public void printTableTest()
        {
            Console.WriteLine("Product Table:");
            var ProductList = Database.Instance().getProduct();
            foreach (var product in ProductList)
            {
                Console.WriteLine($"{product.Id}---{product.Name}---{product.CategoryId}");
            }
        }
    }
}
