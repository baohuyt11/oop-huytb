﻿using OOP.DAO;
using OOP.Enity;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Demo
{
    class CategoryDaoDemo
    {
        var categoryDAO = new CategoryDAO();
        public CategoryDAO categoryDAO;

        /// <summary>
        /// Test insert function
        /// Created by: HuyTB (11/8/2022)
        /// </summary>
        public void insertTest()
        {
            Category category = new Category() { Id = 2, Name = "Wee" };
            category.Id = Database.Instance().getCategory().Count + 1;

            bool status = categoryDAO.insert(category);
            if (status == true)
            {
                Console.WriteLine("\nInsert Success");
            }
            else
            {
                Console.WriteLine("\nInsert Failed");
            }
        }

        /// <summary>
        /// Test update function
        /// Created by: HuyTB (11/8/2022)
        /// </summary>
        public void updateTest()
        {
            Category category = new Category() { Id = 2, Name = "Wee" };
            bool status = categoryDAO.update(category);
            if (status == true)
            {
                Console.WriteLine("\nUpdate success");
            }
            else
            {
                Console.WriteLine("\nUpdate failed");
            }
        }

        /// <summary>
        /// Test delete function
        /// Created by: HuyTB (11/8/2022)
        /// </summary>
        public void deleteTest()
        {
            Category category = new Category() { Id = 2, Name = "Wee" };
            bool status = categoryDAO.delete(category);
            if (status == false)
            {
                Console.WriteLine("\nDelete failed");
            }
            else
            {
                Console.WriteLine("\nDelete Success");
            }
        }

        /// <summary>
        /// Test find all function
        /// Created by: HuyTB (11/8/2022)
        /// </summary>
        public void findAllTest()
        {
            List<object> objects = new List<object>();
            objects.Add(categoryDAO.findAll());
            foreach (Category category in objects)
            {
                Console.WriteLine($"{category.Id}-----{category.Name}");
            }
        }
    }
}
