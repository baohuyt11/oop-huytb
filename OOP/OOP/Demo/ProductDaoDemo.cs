﻿using OOP.DAO;
using OOP.Enity;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Demo
{
    class ProductDaoDemo
    {
        var productDao = new ProductDao()

        /// <summary>
        /// Test insert function
        /// Created by: HuyTB (11/8/2022)
        /// </summary>
        public void insertTest()
        {
            Product product = new Product() { Id = 2, Name = "Banana", CategoryId = 1 };
            product.Id = Database.Instance().getProduct().Count + 1;

            bool status = productDao.insert(product);
            if (status == true)
            {
                Console.WriteLine("\nInsert Success");
            }
            else
            {
                Console.WriteLine("\nInsert Failed");
            }
        }

        /// <summary>
        /// Test update function
        /// Created by: HuyTB (11/8/2022)
        /// </summary>
        public void updateTest()
        {
            Product product = new Product() { Id = 2, Name = "Wee" };
            bool status = productDao.update(product);
            if (status == true)
            {
                Console.WriteLine("\nUpdate success");
            }
            else
            {
                Console.WriteLine("\nUpdate failed");
            }
        }

        /// <summary>
        /// Test delete function
        /// Created by: HuyTB (11/8/2022)
        /// </summary>
        public void deleteTest()
        {
            Product product = new Product() { Id = 2, Name = "Wee" };
            bool status = productDao.delete(product);
            if (status == false)
            {
                Console.WriteLine("\nDelete failed");
            }
            else
            {
                Console.WriteLine("\nDelete Success");
            }
        }
        /// <summary>
        /// Test find all function
        /// Created by: HuyTB (11/8/2022)
        /// </summary>
        public void findAllTest()
        {
            List<object> objects = new List<object>();
            objects.Add(productDao.findAll());
            foreach (Product product in objects)
            {
                Console.WriteLine($"{product.Id}-----{product.Name}-----{product.CategoryId}");
            }
        }
    }
}
