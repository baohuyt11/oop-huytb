﻿using System;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Product Demo");
            ProductDemo PDdemo = new ProductDemo();
            PDdemo.createProductTest();
            PDdemo.printProduct(PDdemo.product);


            Console.WriteLine("Database Product demo");
            DatabaseDemo DBdemo = new DatabaseDemo();
            DBdemo.insertTableTest();
            DBdemo.printTableTest();

            DBdemo.selectTableTest();

            DBdemo.updateTableTest();

            DBdemo.updateTableByIdTest();

            DBdemo.deleteTableTest();

            DBdemo.truncateTableTest();


            Console.WriteLine("Category DAO Demo");
            CategoryDAODemo CDAO = new CategoryDAODemo();
            CDAO.insertTest();
            CDAO.updateTest();
            CDAO.deleteTest();

        }
    }
}
